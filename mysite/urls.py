from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^logout/$', views.logout, name='logout', kwargs={'next_page': '/'}),
    url(r'', include('sample.urls')),
]